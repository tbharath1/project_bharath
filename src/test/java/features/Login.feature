Feature: Login

  @smoke
  Scenario: Login web page
    Given I am on landing page
    When login to application
    Then validate the home page title

  @regression
  Scenario: Login Bank page
    Given I am on landing page
    When Signin to application
      | Username | Password |
      | Bharath  |     1234 |
      | Jo       |     1234 |
