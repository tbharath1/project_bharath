package StepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {
	
	/**
	 * Hooks can run even the test fails
	 * 
	 */
	
	//Normal Hooks 
	
	@Before
	public void Hook1() {
		
		System.out.println("Running Before @smoke test");
	}
	
	@After
	public void Hook2() {
		
	}
	
	// These are tagged Hooks , will run only before or after for tagged scenario
	
	@Before("@smoke")
	public void RunAsHook() {
		
		System.out.println("Running Before @smoke test");
	}
	
	@After
	public void RunAsAfterHook() {
		
	}
	
	//Ordered Hooks, order=0 will run first and like that 

	@Before(order=0)
	public void Hook3() {
		
		System.out.println("Running Before @smoke test");
	}
	
	@After(order=0)
	public void Hook4() {
		
	}
	
}
