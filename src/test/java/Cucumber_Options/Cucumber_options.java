package Cucumber_Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(
		
		features="src/test/java/features/",
		glue="StepDefinitions",
		stepNotifications = true,
		// tags="not @smoke,@sanity,@smoke or @regression"
		tags= "@smoke or @regression",
		plugin= {"pretty","html:target/Cucumber.html", "json:target/Cucumber.json"},
		publish=true
		)
public class Cucumber_options {
	
	


}
